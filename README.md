# scientist

SiLA server for interaction with a scientist - workflow manager

## Features

## Installation

    pip install scientist --index-url https://gitlab.com/api/v4/projects/<gitlab-project-id>/packages/pypi/simple

## Usage

    scientist --help 

## Development

    git clone gitlab.com/opensourcelab/scientist

    # create a virtual environment and activate it then run

    pip install -e .[dev]

    # run unittests

    invoke test   # use the invoke environment to manage development
    

## Documentation

The Documentation can be found here: [https://opensourcelab.gitlab.io/scientist](https://opensourcelab.gitlab.io/scientist) or [scientist.gitlab.io](scientist.gitlab.io/)


## Credits

This package was created with [Cookiecutter](https://github.com/audreyr/cookiecutter)
 and the [gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage](https://gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage) project template.



