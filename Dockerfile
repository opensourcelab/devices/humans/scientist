ARG PYTHON_BASE

FROM python:${PYTHON_BASE}-slim-bookworm as base
ARG PYPI_URL

RUN mkdir /scientist
WORKDIR /scientist

# copy build files
COPY pyproject.toml poetry.lock README.md /scientist/
COPY scientist /scientist/scientist

ENV PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    POETRY_NO_INTERACTION=1 \
    POETRY_VIRTUALENVS_CREATE=0 \
    PIP_INDEX_URL=$PYPI_URL \
    PIP_CACHE_DIR=/var/cache/pip \
    POETRY_CACHE_DIR=/var/cache/poetry

#RUN sed -i 's|http://|https://artifactory.aws.gel.ac/artifactory/apt_|g' /etc/apt/sources.list

RUN \
    --mount=type=cache,target=/var/cache/apt \
    apt-get update -qq && apt-get install -qqy -f \
    git \
    pip

RUN \
    --mount=type=cache,target=${PIP_CACHE_DIR},sharing=private \
    mkdir -p ${PIP_CACHE_DIR} && \
    pip install -Iv --prefer-binary --index-url $PYPI_URL --upgrade \
    poetry==1.8.3

RUN \
    --mount=type=cache,target=${POETRY_CACHE_DIR},sharing=private \
    mkdir -p ${POETRY_CACHE_DIR} && \
    poetry install --only main

FROM base as test
ARG PYPI_URL

WORKDIR /scientist
COPY tests /scientist/tests

# required to make sure pytest runs the right coverage checks
ENV PYTHONPATH .

RUN \
    --mount=type=cache,target=${POETRY_CACHE_DIR},sharing=private \
    poetry install
